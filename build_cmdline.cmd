@echo off
rem Vars
set "msbuild_14=c:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"
set fw_path=c:\Windows\Microsoft.NET\Framework\v4.0.30319

rem Exec
if "%1"=="install" (
    nuget restore -MSBuildVersion 14
)
"%msbuild_14%" -p:FrameworkPathOverride="%fw_path%"
