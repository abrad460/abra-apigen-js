# Popis Ax API - filter:

Conditions of restriction - in the URL address is added ?filter=JSON text encoded to use URL address  

JSON conditions looks like this 

    {"#type": "OR", "Code#eq":"001" , "#":{"DocDate$DATE#gt": 123456789, "DocDate$DATE#lt": 987654321}} 

## Explanation of each parts are below: 

Each field name can contain comparator. Comparator is separated from field name using `#`
 
Comparators can be:
 
- EQ (equals) 
- LT (Less than) 
- LTE (Less than or equal) 
- GT (Greater than) 
- GTE (Greater than or equal) 
- GL (Greate than or Less than) this is similar as "not equal" 
- IN (in) is included in the list. For that type field value must be an array type. Example: {"Code#in":["001","002","003"]} 
- NIN (not in) same as "in" condition, but is negated 
- LIKE vyhovuje zadanej maske. Napr. * abc nájde všetky ktorý končí na abc, abc * nájde všetky ktoré začínajú na abc. 
- CONT (contains) nájde všetky ktoré obsahujú (kdekoľvek) zadaný text. 

If comparator is not used, default is `EQ`. 

Next, after comparator is possible to specify modificator 

Now there is only one modificator, and it is `#uc`, which means upper case. 

    {"Code#eq#uc":"00A"} 

* If field name is `#type`, then this field is not used for restriction, but this field specify type of restrictions combination. 
* If `#type` is not used, default value is `AND` 

    {"ID":"1000000101", "ObjVersion":1} means (ID="1000000101") AND (ObjVersion=1) 
    {"#type":"OR", "Code":"01", "Name":"test"} means (Code="01") OR (Name="test") 

If field name is `#`, it must be followed by JSON object, which specify sub condition
 
    {"#type":"OR", "Code":"01", "#":{"ID":"1000000101", "ObjVersion":1} } 

means 

    (Code="01") OR ((ID="1000000101") AND (ObjVersion=1)) 
