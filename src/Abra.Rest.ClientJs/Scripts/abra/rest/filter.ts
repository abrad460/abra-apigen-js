﻿module Abra.Rest {

    export class Filter {
        public logic = FilterLogic.and;
        public items: Array<FilterItem>;
        public filters: Array<Filter>;

        /**
         * Constructor
         */
        constructor(logic?: FilterLogic) {
            this.logic = logic;
            this.items = new Array<FilterItem>();
            this.filters = new Array<Filter>();
        }

        withFilterItems(filterItems: Array<FilterItem>) {
            filterItems.forEach(x => this.items.push(x));

            return this;
        }

        withFilters(filters: Array<Filter>) {
            filters.forEach(x => this.filters.push(x));

            return this;
        }

        isEmpty() {
            for (let i = 0; i < this.filters.length; i++) {
                if (!this.filters[i].isEmpty()) {
                    return false;
                }
            }

            return (this.items.length === 0);
        }

        compose() {
            if ((!this.items || this.items.length === 0) &&
                (!this.filters || this.filters.length === 0)) {
                return null;
            }

            const logic = this.logic === FilterLogic.or ? ' or ' : ' and ';

            const fcPrefix = (this.filters.length && (this.filters.length > 1 || this.filters[0].items.length > 1)) ? '(' : '';
            const fcSuffix = (this.filters.length && (this.filters.length > 1 || this.filters[0].items.length > 1)) ? ')' : '';
            const filterConditions = (!this.filters.length) ? '' :
                fcPrefix
                + this.filters
                    .map(x => fcPrefix + x.compose() + fcSuffix)
                    .join(logic)
                + fcSuffix;

            const items = this.items.map(x => x.compose()).join(logic);

            if (items) {
                return (items + ((!filterConditions) ? '' : logic + filterConditions));
            }
            else {
                return (filterConditions) ? filterConditions : null;
            }
        }
    }
}