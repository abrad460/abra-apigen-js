﻿module Abra.Rest {

    export interface IFilterItem {
        key: string,
        value: any,
        operator: FilterOperator
    }

    export class FilterItem implements IFilterItem {
        public key: string;
        public value: any;
        public operator: FilterOperator;
        public caseSensitive: boolean;

        /**
         * Constructor
         * @param {string} key
         * @param {any} value
         * @param {FilterOperator} operator
         * @param {boolean} caseSensitive - default false
         */
        constructor(key: string, value: any, operator: FilterOperator, caseSensitive?: boolean) {
            this.key = key;
            this.value = value;
            this.operator = operator;
            this.caseSensitive = (caseSensitive === undefined) ? true : caseSensitive;
        }

        public compose() {
            if (!this.key) return null;

            let value = this.value;
            let key = this.key;
            const valType = typeof value;

            if (this.operator === FilterOperator.in || this.operator === FilterOperator.nin) {
                if (valType === "string") {
                    value = '(' + FilterItem.encode_string(value) + ')';
                }
                else if (valType === "number") {
                    value = '(' + value + ')';
                }
                else {
                    const v = (<Array<any>>value).reduce((prev, current) => {
                        const separator = (prev.length > 0) ? ',' : '';
                        return prev + separator + FilterItem.encode_string("" + current);
                    }, "");

                    value = '(' + v + ')';
                }
            }
            else if (valType === "string") {
                const suffix = '$date';
                if (key.toLowerCase().indexOf(suffix, key.length - suffix.length) !== -1) {
                    value = 'timestamp' + FilterItem.encode_string(value);
                }
                else {
                    // CI search with upper()
                    if (!this.caseSensitive
                        && this.operator !== FilterOperator.in
                        && this.operator !== FilterOperator.nin)
                    {
                        value = FilterItem.encode_string((<string>this.value).toUpperCase());
                        key = "UPPER(" + key + ")";
                    }
                    else {
                        value = FilterItem.encode_string(value);
                    }
                }
            }
            else if (valType === "number") {
                // ignored
            }
            else if (valType === "boolean") {
                value = (value) ? 'true' : 'false';
            }
            else if (valType === 'object') {
                value = 'null';
            }
            else if (valType === 'undefined') {
                // skip
            }
            else {
                console.log(value, this.operator);
                throw { message: 'Unexpected' };
            }

            if (this.operator === FilterOperator.none) {
                return key;
            }
            
            return '(' + ((this.operator === FilterOperator.nin) ? 'not ' : '') + key + ' ' + this.serializeFilterOperator() + ' ' + value + ')';
        }

        static encode_string(str: string) {
            return "'" + str.replace("'", "''") + "'";
        }

        private serializeFilterOperator(): string {
            const fo = this.operator;
            switch (fo) {
                case FilterOperator.none:
                    return '';
                case FilterOperator.eq:
                    return 'eq';
                case FilterOperator.in:
                    return 'in';
                case FilterOperator.like:
                    return 'like';
                case FilterOperator.lt:
                    return 'lt';
                case FilterOperator.lte:
                    return 'le';
                case FilterOperator.gt:
                    return 'gt';
                case FilterOperator.gte:
                    return 'ge';
                case FilterOperator.gl:
                    return 'ne';
                case FilterOperator.nin:
                    return 'in';// gets negated
                case FilterOperator.cont:
                    throw { message: 'Unexpected FilterOperator' };// ?! probably contains str
                default:
                    throw { message: 'Unexpected FilterOperator' };
            }
        }
    }
}