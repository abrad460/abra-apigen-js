﻿module Abra.Rest {

    export enum SortDirection {
        asc,
        desc
    }
}