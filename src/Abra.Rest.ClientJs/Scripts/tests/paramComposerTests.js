/// <reference path="../typings/jasmine/jasmine.d.ts" />
/// <reference path="../abra/rest/paramcomposer.ts" />
/// <reference path="../abra/rest/connection.ts" />
/// <reference path="../abra/rest/client.ts" />
/// <reference path="../abra/rest/responseItems.ts" />
/// <reference path="../abra/rest/sortDirection.ts" />
/// <reference path="../abra/rest/filterItem.ts" />
/// <reference path="../abra/rest/filterLogic.ts" />
/// <reference path="../abra/rest/filterOperator.ts" />
/// <reference path="../abra/rest/filter.ts" />
describe("Param Composer tests", function () {
    var connection = {
        connectorUrl: "https://portal.abra.eu/api/rest/testapi:data"
    };
    it("withFields array", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFields(["firstname", "lastname"]);
        expect("?fields=firstname,lastname").toEqual(composer.compose());
    });
    it("withFields string", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFields("firstname,lastname");
        expect("?fields=firstname,lastname").toEqual(composer.compose());
    });
    it("withAddFields array", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withAddFields(["DocumentNumber", "NotPaidAmount", "PaidStatus"]);
        expect("?addfields=DocumentNumber,NotPaidAmount,PaidStatus").toEqual(composer.compose());
    });
    it("withAddFields string", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withAddFields("DocumentNumber,NotPaidAmount,PaidStatus");
        expect("?addfields=DocumentNumber,NotPaidAmount,PaidStatus").toEqual(composer.compose());
    });
    it("withExpand", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"]);
        expect("?expandfields=electronicaddress_id,firmoffices").toEqual(composer.compose());
    });
    it("withExpand businessObject", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"])
            .withExpandBo("eu.abra.ns.gx.cmpdef.BOFirmOffice", "address_id");
        var actual = "?expandfields=electronicaddress_id,firmoffices&expandfields(eu.abra.ns.gx.cmpdef.BOFirmOffice)=address_id";
        expect(actual).toEqual(composer.compose());
    });
    it("withCount", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withCount(25);
        expect("?count=25").toEqual(composer.compose());
    });
    it("withSkip", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withSkip(101);
        expect("?skip=101").toEqual(composer.compose());
    });
    it("withOrder ASC", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withOrder("name");
        expect("?order=name%23ASC").toEqual(composer.compose());
    });
    it("withOrder DESC", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withOrder(["name", "price"], Abra.Rest.SortDirection.desc);
        expect("?order=name,price%23DESC").toEqual(composer.compose());
    });
    it("withFilter caseInsensitive", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "abra*", Abra.Rest.FilterOperator.like)
        ]));
        expect('?filter={"name%23like%23uc":"ABRA*"}').toEqual(composer.compose());
    });
    it("withFilter caseSensitive", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "Abraham*", Abra.Rest.FilterOperator.like, true)
        ]));
        expect('?filter={"name%23like":"Abraham*"}').toEqual(composer.compose());
    });
    it("withFilter composite", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
            new Abra.Rest.FilterItem("code", "2*", Abra.Rest.FilterOperator.like),
            new Abra.Rest.FilterItem("price", 10.2, Abra.Rest.FilterOperator.eq)
        ]));
        expect('?filter={"name%23like%23uc":"ABRA*","code%23like%23uc":"2*","price%23eq":10.2}').toEqual(composer.compose());
    });
    it("withFilter boolean", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("hidden", false, Abra.Rest.FilterOperator.eq),
            new Abra.Rest.FilterItem("visible", true, Abra.Rest.FilterOperator.eq)
        ]));
        expect('?filter={"hidden%23eq":false,"visible%23eq":true}').toEqual(composer.compose());
    });
    it("withFilter number", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("price", 10.5, Abra.Rest.FilterOperator.eq)
        ]));
        expect('?filter={"price%23eq":10.5}').toEqual(composer.compose());
    });
    it("withFilter OR", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
            new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
        ]));
        expect('?filter={"%23type":"OR","name%23like%23uc":"ABRA*","number%23lt":10}').toEqual(composer.compose());
    });
    it("withFilter innerFilter", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
            new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
        ])
            .withFilters([new Abra.Rest.Filter()
                .withFilterItems([
                new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
            ])
        ]));
        expect('?filter={"%23type":"OR","name%23like%23uc":"ABRA*","number%23lt":10,"%23":{"ID%23eq%23uc":"1000000101","ObjVersion%23lt":1}}').toEqual(composer.compose());
    });
    it("withFilter innerFilters", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
            new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
            new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
        ])
            .withFilters([new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilters([
                new Abra.Rest.Filter()
                    .withFilterItems([
                    new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                    new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                ]),
                new Abra.Rest.Filter()
                    .withFilterItems([
                    new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                    new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                ])
            ])
        ]));
        var expectData = '?filter={"%23type":"OR","name%23like%23uc":"ABRA*","number%23lt":10,"%23":{"%23type":"OR","%23":{"ID%23eq%23uc":"1000000101","ObjVersion%23lt":1},"%23":{"ID%23eq%23uc":"1000000102","ObjVersion%23lt":2}}}';
        expect(expectData).toEqual(composer.compose());
    });
    it("withFilter innerFilters2", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilters([
            new Abra.Rest.Filter()
                .withFilterItems([
                new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
            ]),
            new Abra.Rest.Filter()
                .withFilterItems([
                new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
            ])
        ]));
        var expectData = '?filter={"%23type":"OR","%23":{"ID%23eq%23uc":"1000000101","ObjVersion%23lt":1},"%23":{"ID%23eq%23uc":"1000000102","ObjVersion%23lt":2}}';
        expect(expectData).toEqual(composer.compose());
    });
    it("withFilter emptyFilter", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([]));
        expect('?').toEqual(composer.compose());
    });
    it("aggregate withSum, groupBy", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withFields("amount,localamount,currency_id.code")
            .aggregate.withSum()
            .withGroupBy("currency_id");
        expect("$sum?fields=amount,localamount,currency_id.code&groupby=currency_id").toEqual(composer.compose());
    });
    it("aggregate withCount", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like)]))
            .aggregate.withCount();
        expect('$count?filter={"name%23like%23uc":"ABRA*"}').toEqual(composer.compose());
    });
    it("withFilterOperator in array", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", ["001", "002", "003"], Abra.Rest.FilterOperator.in)
        ]));
        expect('?filter={"code%23in":["001","002","003"]}').toEqual(composer.compose());
    });
    it("withFilterOperator in string", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.in)
        ]));
        expect('?filter={"code%23in":["001"]}').toEqual(composer.compose());
    });
    it("withFilterOperator in number", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.in)
        ]));
        expect('?filter={"code%23in":[5]}').toEqual(composer.compose());
    });
    it("withFilterOperator nin array", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", ["001", "002", "003"], Abra.Rest.FilterOperator.nin)
        ]));
        expect('?filter={"code%23nin":["001","002","003"]}').toEqual(composer.compose());
    });
    it("withFilterOperator nin string", function () {
        var client = new Abra.Rest.Client(connection);
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.nin)
        ]));
        expect('?filter={"code%23nin":["001"]}').toEqual(composer.compose());
    });
    it("withFilterOperator nin number", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
        ]));
        expect('?filter={"code%23nin":[5]}').toEqual(composer.compose());
    });
    it("withFulltext", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withFulltext("abrakadabra");
        expect('?fulltext=abrakadabra').toEqual(composer.compose());
    });
    it("with filter and fulltext", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
        ]))
            .withFulltext("abrakadabra");
        expect('?filter={"code%23nin":[5]}&fulltext=abrakadabra').toEqual(composer.compose());
    });
    it("with filter without FilterOperator", function () {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");
        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
            new Abra.Rest.FilterItem(":type", "OR", Abra.Rest.FilterOperator.none),
            new Abra.Rest.FilterItem("firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
            new Abra.Rest.FilterItem("firm_id.firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
        ]));
        expect('?filter={":type":"OR","firm_id":"Y4I0100101","firm_id.firm_id":"Y4I0100101"}').toEqual(composer.compose());
    });
});
//# sourceMappingURL=paramComposerTests.js.map