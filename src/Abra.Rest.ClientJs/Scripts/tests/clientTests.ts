﻿/// <reference path="../typings/jasmine/jasmine.d.ts" />
/// <reference path="../abra/rest/paramcomposer.ts" />
/// <reference path="../abra/rest/connection.ts" />
/// <reference path="../abra/rest/client.ts" />
/// <reference path="../abra/rest/responseItems.ts" />

describe("ABRA AX REST CLIENT test", () => {
    var connection = <Abra.Rest.IConnection>{
        connectorUrl: "https://portal.abra.eu/api/rest/testapi:data"
    };

    // it("get firms", (done) => {
    //     var client = new Abra.Rest.Client(connection);
    //     var route = client.getRoute("firms");

    //     client.get<Abra.Rest.IResponseItems<Array<any>>>(route)
    //         .then(response => {
    //             var responseItems = new Abra.Rest.ResponseItemsHelper(response);

    //             expect(20).toBe(responseItems.count());
    //             expect(true).toBe(responseItems.nextPage());
    //             expect(true).toBe(responseItems.items.length > 0);

    //             console.log("DONE");
    //             done();
    //         })
    //         .fail((err) => {
    //             console.log("ERROR");
    //             console.log(err);
    //         });

    //     expect(true).toEqual(true);
    // });

    // it("get firms with count 1", (done) => {
    //     var client = new Abra.Rest.Client(connection);
    //     var route = client.getRoute("firms");

    //     var param = new Abra.Rest.ParamComposer().withCount(1);

    //     client.get<Abra.Rest.IResponseItems<Array<any>>>(route, param)
    //         .then(response => {
    //             var responseItems = new Abra.Rest.ResponseItemsHelper(response);

    //             expect(1).toBe(responseItems.count());
    //             expect(true).toBe(responseItems.nextPage());
    //             expect(true).toBe(responseItems.items.length === 1);

    //             console.log("DONE");
    //             done();
    //         })
    //         .fail((err) => {
    //             console.log("ERROR");
    //             console.log(err);
    //         });

    //     expect(true).toEqual(true);
    // });

    it("authorization base64 ASCII", () => {
        var secureConnection = <Abra.Rest.IConnection>JSON.parse(JSON.stringify(connection));
        secureConnection.username = "admin@foo.com";
        secureConnection.password = "abcde#f12gh_*R";
        secureConnection.authorization = Abra.Rest.AuthorizationKind.Basic;
        
        var base64 = btoa(secureConnection.username + ":" + secureConnection.password);

        expect("YWRtaW5AZm9vLmNvbTphYmNkZSNmMTJnaF8qUg==").toEqual(base64);
    });
});