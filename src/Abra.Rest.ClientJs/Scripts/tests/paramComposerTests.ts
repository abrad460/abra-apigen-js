﻿/// <reference path="../typings/jasmine/jasmine.d.ts" />
/// <reference path="../abra/rest/paramcomposer.ts" />
/// <reference path="../abra/rest/connection.ts" />
/// <reference path="../abra/rest/client.ts" />
/// <reference path="../abra/rest/responseItems.ts" />
/// <reference path="../abra/rest/sortDirection.ts" />
/// <reference path="../abra/rest/filterItem.ts" />
/// <reference path="../abra/rest/filterLogic.ts" />
/// <reference path="../abra/rest/filterOperator.ts" />
/// <reference path="../abra/rest/filter.ts" />

describe("Param Composer tests", () => {
    const connection = <Abra.Rest.IConnection>{
        connectorUrl: "https://portal.abra.eu/api/rest/testapi:data"
    };

    it("withFields array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFields(["firstname", "lastname"]);

        expect("?select=firstname%2Clastname").toEqual(composer.compose());
    });

    it("withFields string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFields("firstname,lastname");

        expect("?select=firstname%2Clastname").toEqual(composer.compose());
    });

    it("withAddFields array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withAddFields(["DocumentNumber", "NotPaidAmount", "PaidStatus"]);

        expect("?select=DocumentNumber%2CNotPaidAmount%2CPaidStatus").toEqual(composer.compose());
    });

    it("withAddFields string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withAddFields("DocumentNumber,NotPaidAmount,PaidStatus");

        expect("?select=DocumentNumber%2CNotPaidAmount%2CPaidStatus").toEqual(composer.compose());
    });

    it("withExpand", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"]);

        expect("?expand=electronicaddress_id%2Cfirmoffices").toEqual(composer.compose());
    });

    it("withExpand businessObject", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"])
            .withExpandBo("eu.abra.ns.gx.cmpdef.BOFirmOffice", "address_id");

        var actual = "?expand=electronicaddress_id%2Cfirmoffices%2Caddress_id";
        expect(actual).toEqual(composer.compose());
    });

    it("withCount", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withCount(25);

        expect("?take=25").toEqual(composer.compose());
    });

    it("withSkip", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withSkip(101);

        expect("?skip=101").toEqual(composer.compose());
    });

    it("withOrder ASC", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withOrder("name");

        expect("?orderby=name%20ASC").toEqual(composer.compose());
    });

    it("withOrder DESC", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withOrder(["name", "price"], Abra.Rest.SortDirection.desc);

        expect("?orderby=name%2Cprice%20DESC").toEqual(composer.compose());
    });

    it("withFilter caseInsensitive", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "abra*", Abra.Rest.FilterOperator.like, false)
                ])
            );

        expect("?where=(UPPER(name)%20like%20'ABRA*')").toEqual(composer.compose());
    });

    it("withFilter caseSensitive", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "Abraham*", Abra.Rest.FilterOperator.like, true)
                ])
            );

        expect("?where=(name%20like%20'Abraham*')").toEqual(composer.compose());
    });

    it("withFilter composite", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like, false),
                    new Abra.Rest.FilterItem("code", "2*", Abra.Rest.FilterOperator.like, false),
                    new Abra.Rest.FilterItem("price", 10.2, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect("?where=(UPPER(name)%20like%20'ABRA*')%20and%20(UPPER(code)%20like%20'2*')%20and%20(price%20eq%2010.2)")
            .toEqual(composer.compose());
    });

    it("withFilter boolean", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("hidden", false, Abra.Rest.FilterOperator.eq),
                    new Abra.Rest.FilterItem("visible", true, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect('?where=(hidden%20eq%20false)%20and%20(visible%20eq%20true)').toEqual(composer.compose());
    });

    it("withFilter number", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("price", 10.5, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect('?where=(price%20eq%2010.5)').toEqual(composer.compose());
    });

    it("withFilter OR", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like, false),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
            );

        expect("?where=(UPPER(name)%20like%20'ABRA*')%20or%20(number%20lt%2010)")
            .toEqual(composer.compose());
    });

    it("withFilter innerFilter", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like, false),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
                .withFilters([new Abra.Rest.Filter()
                    .withFilterItems([
                        new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                        new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                    ])
                ])
            );

        expect("?where=(UPPER(name)%20like%20'ABRA*')%20or%20(number%20lt%2010)%20or%20(((ID%20eq%20'1000000101')%20and%20(ObjVersion%20lt%201)))")
            .toEqual(composer.compose());
    });

    it("withFilter innerFilters", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like, false),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
                .withFilters([new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                    .withFilters([
                        new Abra.Rest.Filter()
                            .withFilterItems([
                                new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                                new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                            ]),
                        new Abra.Rest.Filter()
                            .withFilterItems([
                                new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                                new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                            ])
                    ])
                ])
            );

        var expectData = "?where=(UPPER(name)%20like%20'ABRA*')%20or%20(number%20lt%2010)%20or%20(((ID%20eq%20'1000000101')%20and%20(ObjVersion%20lt%201))%20or%20((ID%20eq%20'1000000102')%20and%20(ObjVersion%20lt%202)))";
        expect(expectData).toEqual(composer.compose());
    });

    it("withFilter innerFilters2", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilters([
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                        ]),
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                        ])
                ])
        );

        var expectData = "?where=(((ID%20eq%20'1000000101')%20and%20(ObjVersion%20lt%201))%20or%20((ID%20eq%20'1000000102')%20and%20(ObjVersion%20lt%202)))";
        expect(expectData).toEqual(composer.compose());
    });

    it("withFilter emptyFilter", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([])
            );

        expect('?').toEqual(composer.compose());
    });

    it("aggregate withSum, groupBy", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFields("sum(amount) as amount,sum(localamount) as localamount,currency_id.code")
            .aggregate.withSum()
            .withGroupBy("currency_id");

        expect("?select=sum(amount)%20as%20amount%2Csum(localamount)%20as%20localamount%2Ccurrency_id.code&groupby=currency_id").toEqual(composer.compose());
    });

    it("aggregate withCount", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like, false)]))
            .withFields('count(*)')
            .aggregate.withCount();

        expect("?select=count(*)&where=(UPPER(name)%20like%20'ABRA*')").toEqual(composer.compose());
    });

    it("withFilterOperator in array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", ["001","002","003"], Abra.Rest.FilterOperator.in)
                ])
            );

        expect("?where=(code%20in%20('001'%2C'002'%2C'003'))").toEqual(composer.compose());
    });

    it("withFilterOperator in string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.in)
                ])
            );

        expect("?where=(code%20in%20('001'))").toEqual(composer.compose());
    });

    it("withFilterOperator in number", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.in)
                ])
            );

        expect('?where=(code%20in%20(5))').toEqual(composer.compose());
    });

    it("withFilterOperator nin array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", ["001", "002", "003"], Abra.Rest.FilterOperator.nin)
                ])
            );

        expect("?where=(not%20code%20in%20('001'%2C'002'%2C'003'))").toEqual(composer.compose());
    });

    it("withFilterOperator nin string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.nin)
                ])
            );

        expect("?where=(not%20code%20in%20('001'))").toEqual(composer.compose());
    });

    it("withFilterOperator nin number", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
                ])
            );

        expect('?where=(not%20code%20in%20(5))').toEqual(composer.compose());
    });

    it("withFulltext", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFulltext("abrakadabra");

        expect('?fulltext=abrakadabra').toEqual(composer.compose());
    });

    it("with filter and fulltext", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
                ])
            )
            .withFulltext("abrakadabra");

        expect('?where=(not%20code%20in%20(5))&fulltext=abrakadabra').toEqual(composer.compose());
    });

    // nonsense?!
    // it("with filter without FilterOperator", () => {
    //     var client = new Abra.Rest.Client(connection);
    //     var route = client.getRoute("firms");

    //     var composer = new Abra.Rest.ParamComposer()
    //         .withFilter(new Abra.Rest.Filter()
    //             .withFilterItems([
    //                 new Abra.Rest.FilterItem(":type", "OR", Abra.Rest.FilterOperator.none),
    //                 new Abra.Rest.FilterItem("firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
    //                 new Abra.Rest.FilterItem("firm_id.firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
    //             ])
    //         );

    //     expect('?where=(firm_id)%20or%20(firm_id.firm_id)')
    //         .toEqual(composer.compose());
    // });
});