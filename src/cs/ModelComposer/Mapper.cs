﻿using System;
using System.Linq;
using ModelComposer.Models;
using System.Collections.Generic;

namespace ModelComposer
{
    static class Mapper
    {
        internal static Service GetService(string url)
        {
            using (var webClient = new System.Net.WebClient())
            {
                webClient.Encoding = System.Text.Encoding.UTF8;
                var textData = webClient.DownloadString(url);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Service>(textData);
            }
        }

        /// <summary>
        /// Gets the objects.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        internal static IEnumerable<Bo> GetObjects(Service service, string url)
        {
            var objects = new List<Bo>();

            using (var webClient = new System.Net.WebClient())
            {
                webClient.Encoding = System.Text.Encoding.UTF8;
                service.Services.ToList().ForEach(item =>
                {
                    objects.Add(GetBo(webClient, String.Concat(url, @"/", item.Url, @"/$metadata")));
                });
            }

            return objects;
        }

        static Bo GetBo(System.Net.WebClient webClient, string url)
        {
            var textData = webClient.DownloadString(url);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Bo>(textData);
        }
    }
}
