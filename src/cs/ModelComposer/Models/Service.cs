﻿using Newtonsoft.Json;

namespace ModelComposer.Models
{
    class Service
    {
        [JsonProperty("services")]
        public ServiceItem[] Services { get; set; }
    }
}
