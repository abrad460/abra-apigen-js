﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelComposer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new Program().Process();
                Console.WriteLine("DONE");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        void Process()
        {
            var url = ConfigurationManager.AppSettings["url"] as string;
            var outPath = ConfigurationManager.AppSettings["outputPath"] as string;
            var moduleName = ConfigurationManager.AppSettings["moduleName"] as string;
            var kindText = ConfigurationManager.AppSettings["kind"] as string;
            var kind = (String.IsNullOrEmpty(kindText) || kindText.ToLower().Equals("class")) ? ObjectKind.Class : ObjectKind.Interface;

            var service = Mapper.GetService(url);
            var objects = Mapper.GetObjects(service, url);
            var types = LoadTypeMapper();

            objects.ToList().ForEach(obj =>
            {
                ObjectCreator.CreateFile(obj, System.IO.Path.Combine(outPath, obj.Tablename + ".ts"), types, moduleName, kind);
            });
        }

        Dictionary<string, string> LoadTypeMapper()
        {
            var dict = new Dictionary<string, string>();

            var json = System.IO.File.ReadAllText("mapper.json");
            dynamic data = JObject.Parse(json);
            //var types = data["types"];
            var types = (JArray)data.types;
            foreach(var child in types.Children())
            {
                foreach (JProperty prop in ((JObject)child).Properties())
                {
                    if (String.IsNullOrEmpty(prop.Name))
                        continue;

                    dict.Add(prop.Name, prop.Value.ToString());
                }
            }

            return dict;
        }
    }
}
