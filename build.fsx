// include Fake lib
#r @"packages/FAKE.4.9.3/tools/FakeLib.dll"
open Fake

// Properties
let buildDir = "dist/"

// Targets
Target "Clean" (fun _ ->
    CleanDir buildDir
)

Target "Copy" (fun _ ->
    !! "Readme.md"
    |> CopyTo(buildDir)

    !! "src/Abra.Rest.ClientJs/Changelog.md"
    |> CopyTo(buildDir)

    !! "src/Abra.Rest.ClientJs/publish/*.*"
    |> CopyTo(buildDir)
)

// Dependencies
"Clean"
  ==> "Copy"

// start build
RunTargetOrDefault "Copy"
