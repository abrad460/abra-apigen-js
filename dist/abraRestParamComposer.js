var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var Aggregate = (function () {
            /**
             * Constructor
             */
            function Aggregate(paramComposer) {
                this.paramComposer = paramComposer;
            }
            /**
             * Set $count aggregate function. Default value is 'true'.
             */
            Aggregate.prototype.withCount = function (isCount) {
                this.isCount = isCount || true;
                return this.paramComposer;
            };
            /**
             * Set $sum aggregate function. Default value is 'true'.
             */
            Aggregate.prototype.withSum = function (isSum) {
                this.isSum = isSum || true;
                return this.paramComposer;
            };
            /**
             * Compose parameters to string
             */
            Aggregate.prototype.compose = function () {
                var data = "";
                // nothing like this is needed in Gen API
                // if (this.isCount) {
                //     data = data + "$count";
                // }
                // if (this.isSum) {
                //     data = data + "$sum";
                // }
                return data;
            };
            return Aggregate;
        })();
        Rest.Aggregate = Aggregate;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var Filter = (function () {
            /**
             * Constructor
             */
            function Filter(logic) {
                this.logic = Rest.FilterLogic.and;
                this.logic = logic;
                this.items = new Array();
                this.filters = new Array();
            }
            Filter.prototype.withFilterItems = function (filterItems) {
                var _this = this;
                filterItems.forEach(function (x) { return _this.items.push(x); });
                return this;
            };
            Filter.prototype.withFilters = function (filters) {
                var _this = this;
                filters.forEach(function (x) { return _this.filters.push(x); });
                return this;
            };
            Filter.prototype.isEmpty = function () {
                for (var i = 0; i < this.filters.length; i++) {
                    if (!this.filters[i].isEmpty()) {
                        return false;
                    }
                }
                return (this.items.length === 0);
            };
            Filter.prototype.compose = function () {
                if ((!this.items || this.items.length === 0) &&
                    (!this.filters || this.filters.length === 0)) {
                    return null;
                }
                var logic = this.logic === Rest.FilterLogic.or ? ' or ' : ' and ';
                var fcPrefix = (this.filters.length && (this.filters.length > 1 || this.filters[0].items.length > 1)) ? '(' : '';
                var fcSuffix = (this.filters.length && (this.filters.length > 1 || this.filters[0].items.length > 1)) ? ')' : '';
                var filterConditions = (!this.filters.length) ? '' :
                    fcPrefix
                        + this.filters
                            .map(function (x) { return fcPrefix + x.compose() + fcSuffix; })
                            .join(logic)
                        + fcSuffix;
                var items = this.items.map(function (x) { return x.compose(); }).join(logic);
                if (items) {
                    return (items + ((!filterConditions) ? '' : logic + filterConditions));
                }
                else {
                    return (filterConditions) ? filterConditions : null;
                }
            };
            return Filter;
        })();
        Rest.Filter = Filter;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var FilterItem = (function () {
            /**
             * Constructor
             * @param {string} key
             * @param {any} value
             * @param {FilterOperator} operator
             * @param {boolean} caseSensitive - default false
             */
            function FilterItem(key, value, operator, caseSensitive) {
                this.key = key;
                this.value = value;
                this.operator = operator;
                this.caseSensitive = (caseSensitive === undefined) ? true : caseSensitive;
            }
            FilterItem.prototype.compose = function () {
                if (!this.key)
                    return null;
                var value = this.value;
                var key = this.key;
                var valType = typeof value;
                if (this.operator === Rest.FilterOperator.in || this.operator === Rest.FilterOperator.nin) {
                    if (valType === "string") {
                        value = '(' + FilterItem.encode_string(value) + ')';
                    }
                    else if (valType === "number") {
                        value = '(' + value + ')';
                    }
                    else {
                        var v = value.reduce(function (prev, current) {
                            var separator = (prev.length > 0) ? ',' : '';
                            return prev + separator + FilterItem.encode_string("" + current);
                        }, "");
                        value = '(' + v + ')';
                    }
                }
                else if (valType === "string") {
                    var suffix = '$date';
                    if (key.toLowerCase().indexOf(suffix, key.length - suffix.length) !== -1) {
                        value = 'timestamp' + FilterItem.encode_string(value);
                    }
                    else {
                        // CI search with upper()
                        if (!this.caseSensitive
                            && this.operator !== Rest.FilterOperator.in
                            && this.operator !== Rest.FilterOperator.nin) {
                            value = FilterItem.encode_string(this.value.toUpperCase());
                            key = "UPPER(" + key + ")";
                        }
                        else {
                            value = FilterItem.encode_string(value);
                        }
                    }
                }
                else if (valType === "number") {
                }
                else if (valType === "boolean") {
                    value = (value) ? 'true' : 'false';
                }
                else if (valType === 'object') {
                    value = 'null';
                }
                else if (valType === 'undefined') {
                }
                else {
                    console.log(value, this.operator);
                    throw { message: 'Unexpected' };
                }
                if (this.operator === Rest.FilterOperator.none) {
                    return key;
                }
                return '(' + ((this.operator === Rest.FilterOperator.nin) ? 'not ' : '') + key + ' ' + this.serializeFilterOperator() + ' ' + value + ')';
            };
            FilterItem.encode_string = function (str) {
                return "'" + str.replace("'", "''") + "'";
            };
            FilterItem.prototype.serializeFilterOperator = function () {
                var fo = this.operator;
                switch (fo) {
                    case Rest.FilterOperator.none:
                        return '';
                    case Rest.FilterOperator.eq:
                        return 'eq';
                    case Rest.FilterOperator.in:
                        return 'in';
                    case Rest.FilterOperator.like:
                        return 'like';
                    case Rest.FilterOperator.lt:
                        return 'lt';
                    case Rest.FilterOperator.lte:
                        return 'le';
                    case Rest.FilterOperator.gt:
                        return 'gt';
                    case Rest.FilterOperator.gte:
                        return 'ge';
                    case Rest.FilterOperator.gl:
                        return 'ne';
                    case Rest.FilterOperator.nin:
                        return 'in'; // gets negated
                    case Rest.FilterOperator.cont:
                        throw { message: 'Unexpected FilterOperator' }; // ?! probably contains str
                    default:
                        throw { message: 'Unexpected FilterOperator' };
                }
            };
            return FilterItem;
        })();
        Rest.FilterItem = FilterItem;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (FilterLogic) {
            // Or
            FilterLogic[FilterLogic["or"] = 0] = "or";
            // And
            FilterLogic[FilterLogic["and"] = 1] = "and";
        })(Rest.FilterLogic || (Rest.FilterLogic = {}));
        var FilterLogic = Rest.FilterLogic;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (FilterOperator) {
            // Not used
            FilterOperator[FilterOperator["none"] = 0] = "none";
            // Equals
            FilterOperator[FilterOperator["eq"] = 1] = "eq";
            // "In" is included in the list. For that type field value must be an array type. Example: {"Code#in":["001","002","003"]} 
            FilterOperator[FilterOperator["in"] = 2] = "in";
            // Like
            FilterOperator[FilterOperator["like"] = 3] = "like";
            // Less than
            FilterOperator[FilterOperator["lt"] = 4] = "lt";
            // Less than or equal
            FilterOperator[FilterOperator["lte"] = 5] = "lte";
            // Greater than
            FilterOperator[FilterOperator["gt"] = 6] = "gt";
            // Greater than or equal
            FilterOperator[FilterOperator["gte"] = 7] = "gte";
            // Greate than or Less than. This is similar as "not equal" 
            FilterOperator[FilterOperator["gl"] = 8] = "gl";
            // "Not in" same as "in" condition, but is negated 
            FilterOperator[FilterOperator["nin"] = 9] = "nin";
            // Contains
            FilterOperator[FilterOperator["cont"] = 10] = "cont";
        })(Rest.FilterOperator || (Rest.FilterOperator = {}));
        var FilterOperator = Rest.FilterOperator;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
/// <reference path="aggregate.ts" />
/// <reference path="filter.ts" />
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var ParamComposer = (function () {
            function ParamComposer() {
                this.fields = new Array();
                this.expand = new Array();
                this.order = new Array();
                this.groupBy = new Array();
                this.filter = undefined;
                this.aggregate = new Rest.Aggregate(this);
            }
            /**
             * Append fields criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withFields = function (fields) {
                if (typeof fields === 'string') {
                    this.fields.push(fields);
                }
                else {
                    this.fields.push(fields.join(','));
                }
                return this;
            };
            /**
             * Append addfields criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withAddFields = function (fields) {
                if (typeof fields === 'string') {
                    this.fields.push(fields);
                }
                else {
                    this.fields.push(fields.join(','));
                }
                return this;
            };
            /**
             * Append expand criteria to expand
             * @param {string | Array<string>} expand - address_id
             */
            ParamComposer.prototype.withExpand = function (expand) {
                if (typeof expand === 'string') {
                    this.expand.push(expand);
                }
                else {
                    this.expand.push(expand.join(','));
                }
                return this;
            };
            /**
             * Append expand criteria to expand (businessObject)
             * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
             * @param {string | Array<string>} expand - address_id
             */
            ParamComposer.prototype.withExpandBo = function (bo, expand) {
                if (typeof expand === 'string') {
                    this.expand.push(expand);
                }
                else {
                    this.expand.push(expand.join(','));
                }
                return this;
            };
            /**
             * Append count criteria
             * @param {number} count
             */
            ParamComposer.prototype.withCount = function (count) {
                this.count = count || 0;
                return this;
            };
            /**
             * Append count criteria
             * @param {number} skip
             */
            ParamComposer.prototype.withSkip = function (skip) {
                this.skip = skip || 0;
                return this;
            };
            /**
             * Append order criteria
             * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
             * @param {Abra.Rest.SortDirection} direction - sort direction
             */
            ParamComposer.prototype.withOrder = function (order, direction) {
                if (typeof order === 'string') {
                    this.order.push(order);
                }
                else {
                    this.order.push(order.join(','));
                }
                this.orderKind = direction || Rest.SortDirection.asc;
                return this;
            };
            /**
             * Append filter criteria
             */
            ParamComposer.prototype.withFilter = function (filter) {
                this.filter = filter;
                return this;
            };
            /**
             * Append groupBy criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withGroupBy = function (fields) {
                if (typeof fields === 'string') {
                    this.groupBy.push(fields);
                }
                else {
                    this.groupBy.push(fields.join(','));
                }
                return this;
            };
            /**
             * Set fulltext
             * @param text
             */
            ParamComposer.prototype.withFulltext = function (text) {
                this.fulltext = text;
                return this;
            };
            /**
             * Compose parameters to a URI (string)
             */
            ParamComposer.prototype.compose = function () {
                var _this = this;
                var data = "?";
                // FIELDS
                if (this.fields.length > 0) {
                    this.fields.map(function (item) {
                        data = _this.addComposeItem(data, function () { return "select=" + encodeURIComponent(item); });
                    });
                }
                // EXPAND
                if (this.expand.length > 0) {
                    var expand = "";
                    var comma = "%2C";
                    this.expand.forEach(function (item) {
                        expand = expand + comma + encodeURIComponent(item);
                    });
                    if (expand !== "") {
                        data = this.addComposeItem(data, function () {
                            return "expand=" + expand.substr(comma.length, expand.length - comma.length);
                        });
                    }
                }
                // COUNT
                if (this.count && this.count > 0) {
                    data = this.addComposeItem(data, function () { return "take=" + ((_this.count) ? _this.count : 20); });
                }
                // SKIP
                if (this.skip && this.skip > 0) {
                    data = this.addComposeItem(data, function () { return "skip=" + _this.skip; });
                }
                // ORDER
                if (this.order.length > 0) {
                    this.order.map(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "orderby=" + encodeURIComponent(item + ' ' + Rest.SortDirection[_this.orderKind].toUpperCase());
                        });
                    });
                }
                // FILTER
                if (this.filter && (this.filter.items.length > 0 ||
                    this.filter.filters.filter(function (filter) { return !filter.isEmpty(); }).length > 0)) {
                    data = this.addComposeItem(data, function () {
                        return 'where=' + encodeURIComponent(_this.filter.compose());
                    });
                }
                // GROUPBY
                if (this.groupBy && this.groupBy.length > 0) {
                    this.groupBy.map(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "groupby=" + encodeURIComponent(item);
                        });
                    });
                }
                // FULLTEXT
                if (this.fulltext && this.fulltext.length > 0) {
                    data = this.addComposeItem(data, function () {
                        return "fulltext=" + encodeURIComponent(_this.fulltext);
                    });
                }
                return data; // FIXME
            };
            // Compose param
            ParamComposer.prototype.addComposeItem = function (data, composePart) {
                if (data.length > 1)
                    data = data + "&";
                return data + composePart();
            };
            ParamComposer.Delimiter = ":";
            return ParamComposer;
        })();
        Rest.ParamComposer = ParamComposer;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var ResponseItemsHelper = (function () {
            function ResponseItemsHelper(response) {
                this["@count"] = response["@count"];
                this["@nextpage"] = response["@nextpage"];
                this.items = response.items;
            }
            ResponseItemsHelper.prototype.count = function () {
                return this["@count"] || 0;
            };
            ResponseItemsHelper.prototype.nextPage = function () {
                return this["@nextpage"] || false;
            };
            return ResponseItemsHelper;
        })();
        Rest.ResponseItemsHelper = ResponseItemsHelper;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (SortDirection) {
            SortDirection[SortDirection["asc"] = 0] = "asc";
            SortDirection[SortDirection["desc"] = 1] = "desc";
        })(Rest.SortDirection || (Rest.SortDirection = {}));
        var SortDirection = Rest.SortDirection;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
//# sourceMappingURL=abraRestParamComposer.js.map