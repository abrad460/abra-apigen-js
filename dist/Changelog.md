﻿# Abra.Rest.ClientJs Changelog

## 0.5.8

* FilterOperator.none - možnost nezadání operátora

## 0.5.7

* DELETE - možnost poslání dat v těle požadavku.
* withFulltext - fulltext jako parametr dotazu.

## 0.5.6

* Oprava property v interface IResponseEval

## 0.5.5

* Oprava generování filtru pokud je prázdný, nebo obsahuje vnořený filtr.

## 0.5.4

* postFileRawData - možnost odeslání file

## 0.5.3

* paramComposer - vnitřní stav dostupný přes public property.
* metoda `postFile` - možnost odeslání file

## 0.5.2

* Oprava generování filtru při použití `FilterOperator.nin`

## 0.5.1

* Oprava generování filtru při použití `FilterOperator.in`

## 0.5.0

* Metoda DELETE

##T 0.4.0

* Metoda fnEval

## 0.3.0

* Podpora parametru addfields

## 0.2.0

* Podpora aggregačních funkcí
    * $count
    * $sum
* Podpora `groupby`


## 0.1.0

Podpora `withFields` v paramComposer.

## 0.0.9

Zanořené filtry.

## 0.0.8

Oprava návratového typu z metody POST, PUT

## 0.0.7

Rozšířené možnosti filtrování, FilterOperator, FilterLogic.

## 0.0.6

Přidaná base authorization configuration.

## 0.0.5

Oprava sestavení filtru s parametrem typu boolean.

## 0.0.4

* FilterRule - rozšíření o 'eq'.
* FilterItem - možnost filtrovat caseInsensitive.
* Filtrování podle typu boolean a numeric.

## 0.0.3

Přidaná metoda POST, PUT.

## 0.0.2

Oprava generování paramatru "order". Přidání samostatné knihovny pro generování parametrů.

## 0.0.1

First release
